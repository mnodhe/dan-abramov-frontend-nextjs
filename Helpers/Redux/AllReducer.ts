import { combineReducers } from "redux";
import DarkmodeReducer from "./Reducers/Darkmode/DarkmodeReducer";
import PostsReducer from "./Reducers/Posts/PostsReducer";

const AllReducers = combineReducers({
  UIMode: DarkmodeReducer,
  Posts: PostsReducer,
});
export default AllReducers;

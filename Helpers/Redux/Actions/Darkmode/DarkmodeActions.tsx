import { IRedux_UIMode } from "../../../Interfaces/IRedux";

export const SetUIDarkMode_Action = (data: IRedux_UIMode) => {
  return {
    type: "SetUIDarkMode",
    payload: data,
  };
};

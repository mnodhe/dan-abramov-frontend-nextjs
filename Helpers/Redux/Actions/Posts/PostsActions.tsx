import { PostinitiaState } from "../../Reducers/Posts/PostsReducer";

export const SetPostList_Action = (data: PostinitiaState) => {
  return {
    type: "SetPostList",
    payload: data,
  };
};

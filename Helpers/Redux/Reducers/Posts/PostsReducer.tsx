import { API_GetAllPosts_Response_Result } from "../../../Interfaces/ApiInterface";
import { IRedux_UIMode } from "../../../Interfaces/IRedux";
export interface PostinitiaState {
  postList: API_GetAllPosts_Response_Result[];
}
export let initiaState: PostinitiaState = {
  postList: [],
};
const PostsReducer = (state: PostinitiaState = initiaState, action: any) => {
  switch (action.type) {
    case "SetPostList":
      initiaState.postList = action.payload.postList;
      const newState = Object.assign({}, state, initiaState);
      return newState;
    default:
      return state;
  }
};
export default PostsReducer;

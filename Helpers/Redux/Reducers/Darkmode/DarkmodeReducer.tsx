import { IRedux_UIMode } from "../../../Interfaces/IRedux";

export let DarkMode: IRedux_UIMode = {
  darkmode: false,
};
const DarkmodeReducer = (state: IRedux_UIMode = DarkMode, action: any) => {
  switch (action.type) {
    case "SetUIDarkMode":
      DarkMode.darkmode = action.payload.darkmode;
      const newState = Object.assign({}, state, DarkMode);
      return newState;
    default:
      return state;
  }
};
export default DarkmodeReducer;

import axios from "axios";

// https://jsonplaceholder.typicode.com/posts

export let SiteUrl =
  process.env.NODE_ENV == "development"
    ? "https://jsonplaceholder.typicode.com/"
    : "https://jsonplaceholder.typicode.com/";

const PostsURL = "posts";

let getConfig = (urlParam: string) => {
  let config: any = {
    method: "get",
    url: SiteUrl + urlParam,
    headers: {
      "Content-Type": "application/json",
    },
  };
  return axios(config);
};
let getConfig_SSR = () => {
  let config: any = {
    method: "get",
    url: "https://jsonplaceholder.typicode.com/posts",
    headers: {
      "Content-Type": "application/json",
    },
  };
  return axios(config);
};

export async function API_GetAllPosts() {
  return getConfig(PostsURL);
}
export async function API_GetAllPosts_SSR() {
  return getConfig_SSR();
}

import { useSelector ,TypedUseSelectorHook} from "react-redux";
import { IRedux } from "../Interfaces/IRedux";


export const useAppSelector: TypedUseSelectorHook<IRedux> = useSelector

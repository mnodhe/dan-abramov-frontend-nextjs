export interface API_GetAllPosts_Response_Result {
  userId: number;
  id: number;
  title: string;
  body: string;
}

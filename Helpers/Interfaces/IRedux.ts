import { PostinitiaState } from "../Redux/Reducers/Posts/PostsReducer";

export interface IRedux {
  UIMode: IRedux_UIMode;
  Posts: PostinitiaState;
}
export interface IRedux_UIMode {
  darkmode: boolean;
}

import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import { API_GetAllPosts } from "../ApiHandler";


export function useGetPostList() {
    return useQuery({
        queryKey: ["PostList"],
        queryFn: async () => (await API_GetAllPosts()).data,
    });
}
export function useGetSpecifiedPost(postid: number) {
    return useQuery({
        queryKey: ["SpecifiedPost", postid],
        queryFn: async () => (await API_GetAllPosts()).data.find((x: any) => x.id === postid),
    });
}
import React from 'react'
import { useDispatch } from 'react-redux'
import Toggle, { ToggleIcons } from 'react-toggle'
import { useAppSelector } from '../Helpers/CustomHooks/UseAppSelector'
import { SetUIDarkMode_Action } from '../Helpers/Redux/Actions/Darkmode/DarkmodeActions'
import daylogoswitch from "../public/img/day.png";
import nightlogoswitch from "../public/img/night.png";

export default function Layout({ children }: any) {
    const dispatch = useDispatch()
    const darkmode: boolean = useAppSelector((x) => x.UIMode.darkmode);
    const myToggleIcons: ToggleIcons = {
        unchecked: <img src={daylogoswitch.src} className="col-12" />,
        checked: <img src={nightlogoswitch.src} className="col-12" />,
    };
    return (
        <div className={darkmode ? "bg-dark text-light" : ""}>
            <div className={"LayoutWidth px-5"} >
                <div className="row col-12">
                    <div className="col-lg-8">
                        <header className="Appheader">Overreacted</header>
                    </div>
                    <div className="col-4 text-end align-self-center">
                        <Toggle
                            icons={myToggleIcons}
                            defaultChecked={darkmode}
                            onChange={(e) =>
                                dispatch(SetUIDarkMode_Action({ darkmode: e.target.checked }))
                            }
                        />
                    </div>
                </div>
                {children}
                <footer className="myfooter pb-5">
                    <p>
                        <a
                            className={darkmode ? "pinkcolor" : ""}
                            href="https://mobile.twitter.com/search?q=https%3A%2F%2Foverreacted.io%2Fnpm-audit-broken-by-design%2F"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Discuss on Twitter
                        </a>

                        <span className="fs-7"> • </span>

                        <a
                            className={darkmode ? "pinkcolor" : ""}
                            href="https://github.com/gaearon/overreacted.io/edit/master/src/pages/npm-audit-broken-by-design/index.md"
                            target="_blank"
                            rel="noopener noreferrer"
                        >
                            Edit on GitHub
                        </a>
                    </p>
                </footer>
            </div>
        </div>
    )
}

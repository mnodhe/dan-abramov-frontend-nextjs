import Link from "next/link";
import React from "react";
import { useAppSelector } from "../../Helpers/CustomHooks/UseAppSelector";
import { API_GetAllPosts_Response_Result } from "../../Helpers/Interfaces/ApiInterface";
import { addDays } from "../../Helpers/util/convertor";

export default function Article(props: {
  PostData: API_GetAllPosts_Response_Result;
}) {
  const darkmode: boolean = useAppSelector((x) => x.UIMode.darkmode);

  return (
    <article className="mt-5">
      <header>
        <Link href={"/Post/" + props.PostData.id}>
          <h3 className={darkmode ? "pinkcolor" : ""}>
            {props.PostData.title}
          </h3>
        </Link>
        <small className="Merriweather">{addDays(new Date(), props.PostData.id).toDateString()}</small>
      </header>
      <p>{props.PostData.body}</p>
    </article>
  );
}

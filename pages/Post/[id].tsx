import { Skeleton } from "antd";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import Toggle, { ToggleIcons } from "react-toggle";
import { API_GetAllPosts } from "../../Helpers/ApiHandler";
import { useAppSelector } from "../../Helpers/CustomHooks/UseAppSelector";
import { useGetSpecifiedPost } from "../../Helpers/hooks/usePost";
import { API_GetAllPosts_Response_Result } from "../../Helpers/Interfaces/ApiInterface";
import { SetUIDarkMode_Action } from "../../Helpers/Redux/Actions/Darkmode/DarkmodeActions";
import { SetPostList_Action } from "../../Helpers/Redux/Actions/Posts/PostsActions";
import { addDays } from "../../Helpers/util/convertor";
import daylogoswitch from "../../public/img/day.png";
import nightlogoswitch from "../../public/img/night.png";

export default function () {
  const dispatch = useDispatch();
  const router = useRouter();
  const { id } = router.query;

  // const [CurrentPostData, setCurrentPostData] = useState<
  //   API_GetAllPosts_Response_Result | undefined
  // >({} as API_GetAllPosts_Response_Result);
  // useEffect(() => {
  //   API_GetAllPosts().then((promise) => {
  //     const response: API_GetAllPosts_Response_Result[] = promise.data;
  //     dispatch(SetPostList_Action({ postList: response }));
  //     setLoading(false);
  //   });
  //   return () => { };
  // }, []);

  // useEffect(() => {
  //   setCurrentPostData(AllPostData.find((x) => x.id === Number(id)));
  // }, [Loading === false]);


  const { isLoading, data } = useGetSpecifiedPost(Number(id))

  if (isLoading) {
    return (
      <React.Fragment>
        <Skeleton avatar />
        <Skeleton avatar />
      </React.Fragment>
    )
  }
  return (
    <div >
      <Head>
        <title>{data.title}</title>
      </Head>
      <main className="col-12">
        <article className="mt-5">
          <div className="mt-3">
            <p>{data.body}</p>
          </div>
        </article>
      </main>
    </div>
  );
}

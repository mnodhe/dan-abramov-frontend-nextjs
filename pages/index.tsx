import { Skeleton } from "antd";
import type { InferGetServerSidePropsType, NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import React from "react";
import { useDispatch } from "react-redux";
import Article from "../Components/Home/Article";
import { useAppSelector } from "../Helpers/CustomHooks/UseAppSelector";
import { useGetPostList } from "../Helpers/hooks/usePost";
import { API_GetAllPosts_Response_Result } from "../Helpers/Interfaces/ApiInterface";


export async function getServerSideProps() {
  //For SSR
  let PostDataSSR: API_GetAllPosts_Response_Result[] = [];
  // API_GetAllPosts_SSR().then((promise) => {
  //   const response: API_GetAllPosts_Response_Result[] = promise.data;
  //   PostDataSSR = response;
  // });
  return {
    props: { PostDataSSR }, // will be passed to the page component as props
  };
}

export default function Home({
  PostDataSSR,
}: InferGetServerSidePropsType<typeof getServerSideProps>) {
  // const [Loading, setLoading] = useState(true);

  const dispatch = useDispatch();
  const darkmode: boolean = useAppSelector((x) => x.UIMode.darkmode);
  const PostDataCSR: API_GetAllPosts_Response_Result[] = useAppSelector(
    (x) => x.Posts.postList
  );
  const { data, isLoading } = useGetPostList()
  // useEffect(() => {
  //   API_GetAllPosts().then((promise) => {
  //     const response: API_GetAllPosts_Response_Result[] = promise.data;
  //     dispatch(SetPostList_Action({ postList: response }));
  //     setLoading(false);
  //   });
  //   return () => { };
  // }, []);

  return (
    <div>
      <Head>
        <meta charSet="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no"
        />
        <link
          rel="icon"
          href="/icons/icon-48x48.png?v=8c19a894ebc3f54d282a8f2418cf5398"
          type="image/png"
        />
        <link
          rel="manifest"
          href="/manifest.webmanifest"
          crossOrigin="anonymous"
        />
        <link
          rel="apple-touch-icon"
          sizes="48x48"
          href="/icons/icon-48x48.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="/icons/icon-72x72.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="96x96"
          href="/icons/icon-96x96.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="/icons/icon-144x144.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="192x192"
          href="/icons/icon-192x192.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="256x256"
          href="/icons/icon-256x256.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="384x384"
          href="/icons/icon-384x384.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="512x512"
          href="/icons/icon-512x512.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <title>Overreacted — A blog by Dan Abramov</title>
        <meta
          name="description"
          content="Personal blog by Dan Abramov. I explain with words and code."
        />
        <meta property="og:url" content="https://overreacted.io" />
        <meta property="og:title" content="Overreacted" />
        <meta
          property="og:description"
          content="Personal blog by Dan Abramov. I explain with words and code."
        />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:creator" content="@dan_abramov" />
        <meta name="twitter:title" content="Overreacted" />
        <meta
          name="twitter:description"
          content="Personal blog by Dan Abramov. I explain with words and code."
        />
        <meta name="theme-color" content="#ffa8c5" />
      </Head>

      <aside className="row mt-4">
        <div className="row mx-0 col-12">
          <Image
            className="p-0 rounded-circle"
            src="https://overreacted.io/static/profile-pic-c715447ce38098828758e525a1128b87.jpg"
            alt="Dan Abramov"
            width={60}
            height={10}
          />
          <p className="myparagraph mt-2 px-2">
            Personal blog by
            <a
              className={darkmode ? "pinkcolor" : ""}
              href="https://mobile.twitter.com/dan_abramov"
            >
              {" "}
              Dan Abramov.
            </a>
            <br />I explain with words and code.
          </p>
        </div>
        <main className="mt-1">
          <React.Fragment>
            {isLoading ? (
              <React.Fragment>
                <Skeleton />
                <Skeleton />
                <Skeleton />
                <Skeleton />
              </React.Fragment>
            ) : (
              (data as API_GetAllPosts_Response_Result[]).sort(function (a, b) {
                return b.id - a.id;
              }).map((data, index) => {
                return <Article key={data.id} PostData={data} />;
              })
            )}
          </React.Fragment>
        </main>
      </aside>
    </div>
  );
}
